import React from "react";
import CustomNavbar from "./CustomNavbar";



function Base({children}){
    return(

        <div>
            <CustomNavbar/>
            <div style={{marginTop:'5%'}}>
                {children}
            </div>
               
            <div >
            
            </div>

        </div>
    
    )

}

export default Base;