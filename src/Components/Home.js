

import { useNavigate } from "react-router-dom";
import {card,CardHeader,CardBody, Container,Row,Col, Button} from "reactstrap";
import Base from './Base'
import './home/home.css'

/*we Use Home(Props)  then we have fech dynamically data from  props.title ,props.description,
props.buttonName under return  <h1>{props.title}</h1>> 

but here is problem we can't pass Default value so we use 
object Distrutre 

so inside Home(props) we Use Home({title="Learn code",description="cording",buttonName="like"}) and return
<h1>{title}</h1>

here we set default value
*/




const Home=({title="default titile",description="default Description ",buttonName="Defalut Button",myFun})=>{
    let styleOb={
        padding:'20px',
        background:'#e2e2e2',
        border:'1px solid red',
        margin:'10px'
      }
      const navigate=useNavigate()
      
    return(
        // Dynamic Value By props from App.js

        
/*<h1>{title}</h1>
        <h2>{description}</h2>
        <button onClick={myFun}>{buttonName}</button> */
        //    <div >     
        // <card>
        //     <CardBody>
        //     <h3>{title}</h3>
        //     <p>{description}</p>
        //     </CardBody>
        //    <CardHeader>
        //    <button onClick={myFun}>{buttonName}</button>
        //    </CardHeader>
            
        // </card>
        // </div> 
      

//        <Base >
//        <div className="banner" style={{ backgroundColor: 'blue'}}>
//             <Container >
//                 <Row>
//                     <Col>
//                         <h1 className="text-center" style={{fontWeight:1000,textTransform:"uppercase"}} >Welcome To my Online Store</h1>
//                         <p className="text-center"> Best In Class Experience With Buses Having Extra Comfort & Safety. ₹500 Off* Coupon RED500. Book Bus Tickets Online for 70,000+ Routes Across India With India's No.1 Bus Booking Site.
//                          Reschedulable Tickets. Top Rated App 4.5/5. Live Bus Tracking.
                         
//                          It’s tough to imagine daily life without e-commerce. We order food, clothes, and furniture; we register for classes and other online services; we download books, music, and movies; and so much more. E-commerce has taken root and is here to stay.

// The term “e-commerce” simply means the sale of goods or services on the internet. In its most basic form, e-commerce involves electronically transferring funds and data between 2 or more parties. This form of business has evolved quite a bit since its beginnings in the electronic data interchange of the 1960s and the inception of online shopping in the 1990s.

// In recent years, e-commerce has enjoyed a massive boost from the rise of smartphones, which allow consumers to shop from nearly anywhere. In fact, business experts predicted that mobile e-commerce alone would surpass $284 billion in 2020.

// What is an e-commerce</p>

//                     <Container className="text-center" >
//                         <Button style={{marginBlock:10}} onClick={()=>navigate("/store/all")} className="rounded-0" size="lg" color="success"   >Go to Store</Button>
//                     </Container>
            
//                     </Col>
//                 </Row>
//             </Container>



//         </div>
//        </Base>
        
        <div className="col-sm-12 home-bg-all">
        <Base>
        <div className="banner" style={{ backgroundColor: "blue" }}></div>
        </Base>

        <div className="col-sm-12 p-0 row m-0 ">
        <div className="col-sm-7 m-auto p-0">
            <div className="col-sm-8 m-auto">
            <div className="home-content-1 mb-2">
                Best Products for your Productivity
            </div>
            <div className="home-content-2 mb-2">
                New Tech Collections Trends in 2023
            </div>
            <div className="home-content-3">
                Get the best deals, enjoy fast and free shipping, and shop with
                confidence at our secure online store. Join us now!
            </div>
            </div>
        </div>

        <div className="col-sm-5 p-0">
            <div>
            <img
                src="assets/Home-images/home-img.png"
                alt="Description of the image"
                className="home-img"
            />
            </div>
        </div>
        </div>

        <div className="col-sm-12 p-0">
        <p className="home-heading-names">Featured Products</p>
        </div>

        <div className="col-sm-10 mx-auto p-0">
        <div
            id="carouselExampleIndicators"
            className="carousel slide pb-5"
            data-bs-ride="carousel"
        >
            <div className="carousel-indicators">
            <button
                type="button"
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="0"
                className="active"
                aria-current="true"
                aria-label="Slide 1"
            ></button>
            <button
                type="button"
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="1"
                aria-label="Slide 2"
            ></button>
            <button
                type="button"
                data-bs-target="#carouselExampleIndicators"
                data-bs-slide-to="2"
                aria-label="Slide 3"
            ></button>
            </div>
            <div className="carousel-inner">
            <div className="carousel-item active">
                <div className="col-sm-10 mx-auto row p-0">
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                </div>
            </div>
            <div className="carousel-item">
                <div className="col-sm-10 mx-auto row p-0">
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                </div>
            </div>
            <div className="carousel-item">
                <div className="col-sm-10 mx-auto row p-0">
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                <div className="carousel-w-25">
                    <img
                    src="assets/Home-images/home-img.png"
                    className="d-block w-100 carousel-products"
                    alt="..."
                    />
                    <p>Product Name</p>
                    <p>Price</p>
                </div>
                </div>
            </div>
            </div>
            <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
            >
            <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
            </button>
            <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
            >
            <span
                className="carousel-control-next-icon"
                aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
            </button>
        </div>
        </div>

        <div className="col-sm-12 p-0">
        <p className="home-heading-names">Latest Products</p>
        </div>

        <div className="col-sm-12 p-0">
        <div className="col-sm-9 mx-auto p-0">
            <div className="col-sm-12 p-0 row m-0 justify-content-between">
            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>

            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>

            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        <div className="col-sm-9 mx-auto p-0">
            <div className="col-sm-12 p-0 row m-0 justify-content-between">
            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>

            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>

            <div className="latest-product-card-w-33 p-0">
                <img src="assets/Home-images/home-img.png" className="d-block w-100 carousel-latest-products" alt="..." />
                <div className="row m-0 justify-content-between">
                <div className="w-auto p-0">
                    <p>Product Name</p>
                </div>
                <div className="w-auto p-0">
                    <p>Price</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>

        <div className="col-sm-12 p-0">
        <p className="home-heading-names">Trending Products</p>
        </div>
        </div>
        
    );
}

export default Home